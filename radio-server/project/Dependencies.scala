import sbt._

object Dependencies {

  object Versions {
    val scala = "2.11.7"
    val akka = "2.4.1"
    val logback = "1.1.3"
    val orientDb = "2.1.5"
  }

  object V {
    val depProject = "master"
    // Other library versions
  }

  object Projects {
    lazy val orientPersistence = RootProject(uri("git://github.com/funobjects/akka-persistence-orientdb.git#%s".format(V.depProject)))
  }

  val resolvers = Seq(
    "krasserm at bintray" at "http://dl.bintray.com/krasserm/maven",
    "maven repo" at "http://maven-repository.com/artifact",
    "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/",
    "Eventuate Releases" at "https://dl.bintray.com/rbmhtechnology/maven",
    Resolver.sonatypeRepo("releases")
  )

  lazy val backend = Def.setting(Seq(
    "com.typesafe.akka" %% "akka-actor" % Versions.akka,
    "com.typesafe.akka" %% "akka-agent" % Versions.akka,
    "com.typesafe.akka" %% "akka-slf4j" % Versions.akka,
    "com.typesafe.akka" %% "akka-persistence-query-experimental" % Versions.akka,
    "com.typesafe.akka" % "akka-stream-experimental_2.11" % "1.0",
    "ch.qos.logback" % "logback-core" % Versions.logback,
    "ch.qos.logback" % "logback-classic" % Versions.logback,
    "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2",
    "org.apache.commons" % "commons-lang3" % "3.4",
    "commons-io" % "commons-io" % "2.4",
    "org.scalatest" %% "scalatest" % "2.2.4" % "test",
    "org.mockito" % "mockito-core" % "1.10.19" % "test",
    "com.typesafe.akka" %% "akka-testkit" % Versions.akka % "test",
    //OrientDB
    "com.orientechnologies" % "orientdb-core" % Versions.orientDb,
    "com.orientechnologies" % "orientdb-graphdb" % Versions.orientDb,
    "com.orientechnologies" % "orientdb-object" % Versions.orientDb,
    "com.michaelpollmeier" % "gremlin-scala" % "2.4.2",
    "javax.persistence" % "persistence-api" % "1.0.2"
  ))
}
