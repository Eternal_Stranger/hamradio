package org.stranger.hamradio.backend

import akka.actor.ActorSystem
import org.stranger.hamradio.backend.config.Settings
import org.stranger.hamradio.backend.session.SessionsRegistryActor

/**
 * @author A.Morozov <stranger82@bk.ru>
 */
trait RootActors {

  implicit def system: ActorSystem
  def settings: Settings

  val sessionsRegistry = system.actorOf(SessionsRegistryActor.props(), name = SessionsRegistryActor.actorName)

}
