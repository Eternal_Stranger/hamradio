package org.stranger.hamradio.backend.handlers

import akka.actor.{ActorContext, ActorRef}
import org.stranger.hamradio.backend.config.Settings

/**
  * Created by stranger on 12/3/2015.
  */

trait CommandHandlers {

  protected def authenticationActorRef(settings: Settings, usersActorRef: ActorRef, sessionsStoreActorRef: ActorRef)
                                      (implicit context: ActorContext): ActorRef = {
    context.actorOf(AuthenticationHandlingActor.props(settings, usersActorRef, sessionsStoreActorRef),
      name = s"auth-commands-handling-actor-${Math.random()}")
  }
}
