package org.stranger.hamradio.backend.network

import akka.stream.stage.{TerminationDirective, SyncDirective, Context, PushPullStage}
import akka.util.{ByteString, ByteIterator}

/**
  * @author A.Morozov
  */

private trait IntDecoder {
  def decode(bs: ByteIterator): Int
}

class FramingException(msg: String) extends RuntimeException(msg)

private class BigEndianCodec(val length: Int) extends IntDecoder {
  override def decode(bs: ByteIterator): Int = {
    var count = length
    var decoded = 0
    while (count > 0) {
      decoded <<= 8
      decoded |= bs.next().toInt & 0xFF
      count -= 1
    }
    decoded
  }
}

private class LengthFieldFramingStage(
                                       val lengthFieldLength: Int,
                                       val lengthFieldOffset: Int,
                                       val maximumFrameLength: Int)
  extends PushPullStage[ByteString, ByteString] {
  private var buffer = ByteString.empty
  private val minimumChunkSize = lengthFieldOffset + lengthFieldLength
  private val intDecoder: IntDecoder = new BigEndianCodec(lengthFieldLength)

  private var frameSize = Int.MaxValue

  private def parseLength: Int = intDecoder.decode(buffer.iterator.drop(lengthFieldOffset))

  private def tryPull(ctx: Context[ByteString]): SyncDirective = {
    if (ctx.isFinishing) ctx.fail(new FramingException(
      "Stream finished but there was a truncated final frame in the buffer"))
    else ctx.pull()
  }

  override def onPush(chunk: ByteString, ctx: Context[ByteString]): SyncDirective = {
    buffer ++= chunk
    doParse(ctx)
  }

  override def onPull(ctx: Context[ByteString]): SyncDirective = {
    doParse(ctx)
  }

  override def onUpstreamFinish(ctx: Context[ByteString]): TerminationDirective = {
    if (buffer.nonEmpty) ctx.absorbTermination()
    else ctx.finish()
  }

  private def emitFrame(ctx: Context[ByteString], dataLength: Int): SyncDirective = {
    buffer = buffer.drop(frameSize - dataLength)
    val parsedFrame = buffer.take(dataLength).compact
    buffer = buffer.drop(dataLength)
    frameSize = Int.MaxValue
    if (ctx.isFinishing && buffer.isEmpty) ctx.pushAndFinish(parsedFrame)
    else ctx.push(parsedFrame)
  }

  private def doParse(ctx: Context[ByteString]): SyncDirective = {
    if (buffer.size >= frameSize || !checkSignature(buffer)) {
      //emitFrame(ctx, buffer.size)
      ctx.fail(new FramingException("Tcp framing exception"))
    } else if (buffer.size >= minimumChunkSize) {
      val dataLength = parseLength
      frameSize = dataLength + minimumChunkSize
      if (frameSize > maximumFrameLength)
        ctx.fail(new FramingException(s"Maximum allowed frame size is $maximumFrameLength " +
          s"but decoded frame header reported size $frameSize"))
      else if (buffer.size >= frameSize)
        emitFrame(ctx, dataLength)
      else tryPull(ctx)
    } else tryPull(ctx)
  }
  private def checkSignature(buf: ByteString): Boolean = {
    if (buf.size < 2) {true}
    else buf.head == 'H' && buf.tail.head == 'M'
  }

  override def postStop(): Unit = buffer = null
}