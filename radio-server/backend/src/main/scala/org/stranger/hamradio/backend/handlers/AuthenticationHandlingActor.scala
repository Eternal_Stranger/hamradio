package org.stranger.hamradio.backend.handlers

import akka.actor.{ActorRef, Props}
import org.stranger.hamradio.backend.config.Settings
import org.stranger.hamradio.backend.domain.{UsersActor, User, Id}
import org.stranger.hamradio.backend.handlers.CommandsHandlingActor.Data
import org.stranger.hamradio.backend.network.ServerMessage.ServerResponse
import org.stranger.hamradio.backend.network.ServerRequest
import org.stranger.hamradio.backend.protocol.FailureType

import scala.concurrent.duration._

object AuthenticationHandlingActor {

  case class AuthContextData(request: ServerRequest, originalSenderRef: ActorRef) extends Data

  def props(settings: Settings, usersActor: ActorRef, sessionsStoreActorRef: ActorRef): Props = {
    Props(classOf[AuthenticationHandlingActor], settings, usersActor, sessionsStoreActorRef)
  }

  sealed trait Response
  object Response {
    case class Failure(tpe: FailureType) extends Response
    case class SignedIn(user: Id[User], sessionToken: String) extends Response
    case class CheckResult(status: Boolean, isExpired: Boolean) extends Response
  }
}

class AuthenticationHandlingActor(settings: Settings, usersActor: ActorRef, sessionsStoreActorRef: ActorRef)
  extends CommandsHandlingActor {
  import AuthenticationHandlingActor._
  import CommandsHandlingActor._

  startWith(State.Awaiting, Data.Empty)

  override def stateSwitch[In](in: In, req: ServerRequest, originalSender: ActorRef): State = {
    in match {
      case x: Login ⇒
        goto (State.Working) using AuthContextData (req, originalSender)
      case _ ⇒
        sender ! ServerResponse(req.id, Left(FailureType.InvalidInput))
        stop()
    }
  }

  when(State.Awaiting) {
    case Event(req @ ServerRequest(id, data), Data.Empty) =>
      handleCommand(req, Login.cmd.read(data)) { in ⇒
        usersActor ! UsersActor.Request.ResolveByName(in.username)
      }
  }

  when(State.Working, stateTimeout = 500.millis ) {
    case Event(StateTimeout, wd @ Data.WorkingData(_, originalSender)) ⇒
      originalSender ! Response.Failure(FailureType.Timeout)
      stop()

    case Event(UsersActor.Response.Resolved(Left(failure)), wd @ AuthContextData(terminal, req, originalSender)) ⇒
      originalSender ! Response.Failure(failure)
      stop()

    case Event(UsersActor.Response.Resolved(Right(user @ User(Some(userId), _, password)) ), wd: AuthContextData) ⇒
      sessionsStoreActorRef ! SessionStoreActor.Request.NewSession(user, wd.terminal)
      stay()
  }

  initialize()
}
