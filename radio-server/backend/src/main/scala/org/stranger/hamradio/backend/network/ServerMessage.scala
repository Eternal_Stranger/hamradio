package org.stranger.hamradio.backend.network

import org.stranger.hamradio.backend.domain.{Id, DomainEntity}

/**
  * Created by stranger on 12/3/2015.
  */

trait ServerRequest {
  def id: String
}

sealed trait ServerResponseDetails
object ServerResponseDetails {

  case object Success extends ServerResponseDetails
  case object ObjectDeleted extends ServerResponseDetails

  case class ObjectUpdated[T <: DomainEntity](id: Id[T]) extends ServerResponseDetails

  case class ObjectsUpdated[T <: DomainEntity](id: Seq[Id[T]]) extends ServerResponseDetails

  case class ObjectResolved(obj: DomainEntity) extends ServerResponseDetails

  case class ObjectsListResolved[T <: DomainEntity](lst: Seq[T]) extends ServerResponseDetails

  case class Data(r: String) extends ServerResponseDetails

}

sealed trait ServerMessage
object ServerMessage {
  case class PushMessage(eventType: String, data: Either[Throwable, ServerResponseDetails]) extends ServerMessage
  case class ServerResponse(id: String, data: Either[Throwable, ServerResponseDetails]) extends ServerMessage
}
