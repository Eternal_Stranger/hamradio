package org.stranger.hamradio.backend

import akka.actor.ActorSystem
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.stranger.hamradio.backend.config.Settings
import org.stranger.hamradio.backend.storage.OrientDbConnector

/**
 * @author A.Morozov
 */
object Main extends App with LazyLogging {

  implicit val system = ActorSystem("involve-server")

  logger.info("Radio server starting...")

  val server = new AppServer(new Settings())
  server.start()

  val db = new OrientDbConnector()
  db.test()

}
