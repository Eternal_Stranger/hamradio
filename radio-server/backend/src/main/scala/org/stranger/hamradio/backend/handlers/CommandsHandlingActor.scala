package org.stranger.hamradio.backend.handlers

import akka.actor.{ActorRef, FSM}
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.stranger.hamradio.backend.network.ServerMessage.ServerResponse
import org.stranger.hamradio.backend.network.ServerRequest
import org.stranger.hamradio.backend.protocol.FailureType
import scala.util.{Failure, Success, Try}

object CommandsHandlingActor {
  private[handlers] trait State
  private[handlers] object State {
    case object Awaiting extends State
    case object Working extends State
  }

  private[handlers] trait Data
  private[handlers] object Data {
    case object Empty extends Data
    case class WorkingData(req: ServerRequest, originalSender: ActorRef) extends Data
  }
}

trait CommandsHandlingActor extends FSM[CommandsHandlingActor.State, CommandsHandlingActor.Data]
with LazyLogging {
  import CommandsHandlingActor._

  startWith(State.Awaiting, Data.Empty)

  def stateSwitch[In](in: In, req: ServerRequest, originalSender: ActorRef): State = {
    goto(State.Working) using Data.WorkingData(req, originalSender)
  }

  protected def handleCommand[In](req: ServerRequest, parsedData: ⇒ Try[In])
                                 (handler: In ⇒ Unit) = {
    try {
      parsedData match {
        case Success(in) ⇒
          logger.info(s"Command - ${req} - $in")
          handler(in)
          stateSwitch(in, req, sender())
        case Failure(exception) ⇒
          logger.error("Failed to read input data", exception)
          sender() ! ServerResponse(req.id, Left(exception))
          stop()
      }
    } catch {
      case exception: Exception ⇒
        logger.info("Command execution failed", exception)
        sender() ! ServerResponse(req.id, Left(exception))
        stop()
    }
  }

  whenUnhandled {
    case Event(e: Any, Data.WorkingData(request, senderRef)) ⇒
      logger.error(s"Unknown request on $stateName: " + e)
      senderRef ! ServerResponse(request.id, Left(FailureType.Unknown))
      stop()

    case Event(e: Any, _) ⇒
      logger.error(s"Unknown request on $stateName: " + e)
      stop()
  }
}
