package org.stranger.hamradio.backend.network

import java.nio.ByteOrder
import akka.stream.scaladsl.Flow
import akka.util.ByteString

/**
 * Created by Alexander Morozov <stranger82@bk.ru> on 30.10.15.
 */

trait TransportLayer {

  implicit val byteOrder = ByteOrder.BIG_ENDIAN

  val MaxFrameLength = 10 * 1024 * 1024

  var buffer = None: Option[ByteString]

  def frameDecoder: Flow[ByteString, ByteString, Unit] = {
      lengthField(
        fieldLength = 4,
        fieldOffset = 2,
        maximumFrameLength = MaxFrameLength
      )
  }

  def frameEncoder: Flow[ByteString, ByteString, Unit] = {
    Flow[ByteString].map(frame ⇒ {
      println("[Out]" + frame.toString())
      val builder = ByteString.newBuilder
      builder.append(ByteString("HM"))
      builder.putInt(frame.length)
      builder.append(frame)
      builder.result()
    })
  }


  def lengthField(fieldLength: Int,
                  fieldOffset: Int = 0,
                  maximumFrameLength: Int): Flow[ByteString, ByteString, Unit] = {
    require(fieldLength >= 1 && fieldLength <= 4, "Length field length must be 1, 2, 3 or 4.")
    Flow[ByteString].transform(() ⇒ new LengthFieldFramingStage(fieldLength, fieldOffset, maximumFrameLength))
      .named("lengthFieldFraming")
  }
}
