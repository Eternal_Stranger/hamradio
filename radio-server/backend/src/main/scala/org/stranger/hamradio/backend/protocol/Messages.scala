package org.stranger.hamradio.backend.protocol

/**
  * Created by stranger on 12/3/2015.
  */

//CQRS command
trait ServerCommand {
  def id: String
  def event: ServerEvent
}

object ServerCommand {

}

sealed trait ServerEvent {
  def id: String
}

object ServerEvent {
  //CQRS query
  case class Event(id: String, eventType: String, data: Any) extends ServerEvent
  //response to user request
  case class Response(id: String, data: Any) extends ServerEvent
}
