package org.stranger.hamradio.backend.session

import akka.actor._
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.stranger.hamradio.backend.domain.{User, Id}
import org.stranger.hamradio.backend.protocol.ServerEvent$
import scala.util.{Failure, Success}


/**
  * Created by stranger on 12/3/2015.
  */

object SessionsRegistryActor {
  val actorName: String = "sessionsRegistry"

  def props(): Props = {
    Props(classOf[SessionsRegistryActor])
  }

  def selection()(implicit system: ActorSystem): ActorSelection = {
    system.actorSelection(s"/user/$actorName")
  }

  private[SessionsRegistryActor] sealed trait State
  private[SessionsRegistryActor] object State {
    case object DefaultState extends State
  }

  private[SessionsRegistryActor] sealed trait Data
  private[SessionsRegistryActor] object Data {

    case class ConnectionsData(authenticated: Map[Id[User], Set[ActorRef]]) extends Data

  }

  sealed trait Request
  object Request {
    case class RegisterUserSession(userId: Id[User], session: ActorRef) extends Request
    case class DeleteUserSession(userId: Id[User], session: ActorRef) extends Request
    case class RequestUserSessions(userId: Id[User]) extends Request
  }

  sealed trait Response
  object Response {
    case object UserSessionRegistered extends Response
    case object UserSessionDeleted extends Response
    case object UserResponseSent extends Response
    case class UserSessionsList(sessions: Option[Set[ActorRef]]) extends Response
  }

  case object UserNotFound extends Exception
}

class SessionsRegistryActor extends FSM[SessionsRegistryActor.State, SessionsRegistryActor.Data] with LazyLogging {
  import SessionsRegistryActor.State._
  import SessionsRegistryActor.Data._
  import SessionsRegistryActor._
  import SessionActor.OutMessage._

  startWith(DefaultState, ConnectionsData(Map().withDefaultValue(Set())))

  when(DefaultState) {
    case Event(UserOutMessage(userId, body), ConnectionsData(xs)) ⇒
      val response = xs.get(userId) match {
        case Some(users) ⇒
          users.foreach(_ ! body)
          Success
        case None ⇒
          Failure(UserNotFound)
      }
      stay() replying response

    case Event(Request.RegisterUserSession(userId, session), ConnectionsData(xs)) ⇒
      stay() using ConnectionsData( xs + ( userId → (xs(userId) + session) ) ) replying Response.UserSessionRegistered

    case Event(Request.DeleteUserSession(userId, session), ConnectionsData(xs)) ⇒
      stay() using ConnectionsData( xs + ( userId → (xs(userId) - session) ) ) replying Response.UserSessionRegistered

    //broadcast message to all user connections
    case Event(m: ServerEvent.Event, ConnectionsData(connections)) ⇒
      connections foreach { p ⇒
        p._2 foreach { _ ! m }
      }

      stay()

    case Event(Request.RequestUserSessions(userId), ConnectionsData(xs)) ⇒
      stay() replying Response.UserSessionsList(xs.get(userId))

    case Event(message, data) ⇒
      logger.info(s"Unknown message passed: $message; $data")
      stay()
  }

  initialize()

}