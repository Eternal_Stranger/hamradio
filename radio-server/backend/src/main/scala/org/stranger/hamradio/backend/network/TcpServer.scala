package org.stranger.hamradio.backend.network

import java.util.UUID

import akka.actor.{PoisonPill, ActorRef, ActorSystem}
import akka.stream.scaladsl._
import akka.stream.stage.{Context, PushStage, SyncDirective, TerminationDirective}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.util.ByteString
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.stranger.hamradio.backend.RootActors
import org.stranger.hamradio.backend.session.SessionActor
import org.stranger.hamradio.backend.network.TcpServer.{ConnectionClosed, ConnectionOpen}

import scala.util.{Failure, Success}

/**
 * Created by stranger on 23.10.15.
 */

object TcpServer {

  sealed trait ServerEvent

  case class ConnectionOpen(id: String, subscriber: ActorRef) extends ServerEvent

  case object ConnectionClosed extends ServerEvent

}

class TcpServer(system: ActorSystem, address: String, port: Int, rootActors: RootActors) extends TransportLayer with LazyLogging {

  implicit val sys = system

  import system.dispatcher

  implicit val materializer = ActorMaterializer()

  def start() = {

    val connections = Tcp().bind(address, port)

    val handler = Sink.foreach[Tcp.IncomingConnection] { connection ⇒
      logger.info(s"User connected from ${connection.remoteAddress}")
      val clientId = UUID.randomUUID().toString
      val sessionActor = system.actorOf(SessionActor.props(clientId, rootActors), s"session-$clientId")

      val upstreamErrorFlow: Flow[ByteString, ByteString, Unit] = {

        Flow[ByteString].transform(() ⇒ new PushStage[ByteString, ByteString] {
          def onPush(elem: ByteString, ctx: Context[ByteString]): SyncDirective = ctx.push(elem)
          override def onUpstreamFailure(cause: Throwable, ctx: Context[ByteString]): TerminationDirective = {
            logger.error("User connection failure for # ${connection.remoteAddress} !")
            super.onUpstreamFailure(cause, ctx)
          }
          override def onUpstreamFinish(ctx: Context[ByteString]): TerminationDirective = {
            logger.debug(s"Client ${connection.remoteAddress} disconnected")
            sessionActor ! PoisonPill
            super.onUpstreamFinish(ctx)
          }
        })
      }

      connection.handleWith(upstreamErrorFlow via socketFlow(sessionActor))
    }

    val binding = connections.to(handler).run()

    binding.onComplete {
      case Success(b) ⇒
        logger.info("Server started, listening on: " + b.localAddress)
      case Failure(e) ⇒
        logger.info(s"Server could not bind to $address:$port: ${e.getMessage}")
        system.terminate()
    }

  }

  def socketFlow(sessionActor: ActorRef): Flow[ByteString, ByteString, Unit] = {
    val in = Flow[ByteString].via(frameDecoder).to(Sink.actorRef[ByteString](sessionActor, ConnectionClosed))
    val out = Source.
      actorRef[ByteString](bufferSize = MaxFrameLength, OverflowStrategy.dropHead).
      via(frameEncoder).
      mapMaterializedValue { sessionActor ! ConnectionOpen(UUID.randomUUID().toString, _) }
    Flow.wrap(in, out)(Keep.none)
  }

}
