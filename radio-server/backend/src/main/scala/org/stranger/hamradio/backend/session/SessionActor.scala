package org.stranger.hamradio.backend.session

import akka.actor._
import akka.stream.actor.ActorPublisher
import akka.util.{ByteString, Timeout}
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.omg.CORBA.ServerRequest
import org.stranger.hamradio.backend.RootActors
import org.stranger.hamradio.backend.domain.{User, Id}
import org.stranger.hamradio.backend.handlers.{CommandHandlers, AuthenticationHandlingActor}
import org.stranger.hamradio.backend.network.ServerMessage.ServerResponse
import org.stranger.hamradio.backend.network.ServerRequest
import org.stranger.hamradio.backend.network.{ServerMessage, ServerRequest, TcpServer}
import org.stranger.hamradio.backend.session.SessionActor.State
import org.stranger.hamradio.backend.session.SessionActor._
import org.stranger.hamradio.backend.network.TcpServer.ConnectionOpen
import org.stranger.hamradio.backend.protocol.{FailureType, ServerCommand}
import scala.concurrent.duration._

object SessionActor {

  def props(sessionId: String, rootActors: RootActors): Props = {
    Props(classOf[SessionActor], sessionId, rootActors)
  }


  private[SessionActor] sealed trait State

  private[SessionActor] object State {
    case object NotInitialized extends State
    case object Unauthorized extends State
    case object Progress extends State
    case object Authenticated extends State
  }


  private[SessionActor] sealed trait Data

  private[SessionActor] sealed trait Data
  private[SessionActor] object Data {
    case object Empty extends Data
    case class Authentication(id: String, connection: ActorRef) extends Data
    case class Connection(connection: ActorRef) extends Data
    case class UserConnection(user: Id[User], connection: ActorRef) extends Data
  }

  sealed trait InMessage
  object InMessage {
    case class UserRequest(userId: Id[User], request: ServerCommand) extends InMessage
    case object GetTerminalContext extends InMessage
  }

  sealed trait OutMessage
  object OutMessage {
    case class UserOutMessage(userId: Id[User], body: Any) extends OutMessage
  }

}

class SessionActor(sessionId: String, rootActors: RootActors) extends LoggingFSM[State, Data]
  with ActorPublisher[ByteString] with LazyLogging with CommandHandlers {

  implicit val timeout = Timeout(5.seconds)

  private[SessionActor] val progressStateTimeout = 500.millis

  val MaxQueueSize = 10

  var connection: ActorRef = null

  override def postStop() = {logger.info("Session close")}

  startWith(State.NotInitialized, Data.Empty)

  when(State.NotInitialized) {
    case Event(TcpServer.ConnectionOpen(_, connection), _) ⇒
      goto(State.Unauthorized) using Data.Connection(connection)
  }

  when(State.Unauthorized) {
    case Event(r@ ServerRequest(_, terminals.ServiceName, terminals.List.cmd.Name, data), uc: Data.Connection) ⇒
      terminalsCommandsHandlerRef(rootActors.terminalsActorRef).tell(r, uc.connection)
      stay()

    case Event(req @  ServerRequest(id, auth.ServiceName, auth.Login.cmd.Name, data), Data.Connection(connection)) ⇒
      authenticationActorRef(settings, rootActors.usersActorRef, rootActors.sessionsStoreActorRef) ! req
      goto(State.Progress) using Data.Authentication(id, connection)

    case Event(req: ServerRequest, uc: Data.UserConnection) ⇒
      logger.info(s"Unknown operation requested, ${req.toString}")
      uc.connection ! ServerResponse(req.id, Left(FailureType.UnknownMethod))
      stay()
  }

  when(State.Progress, stateTimeout = progressStateTimeout) {
    case Event(StateTimeout, dc: Data.Connection) ⇒
      goto(State.Unauthorized) using dc

    case Event(AuthenticationHandlingActor.Response.SignedIn(userId, terminalId, token), Data.Authentication(id, connection)) ⇒
      connection ! ServerMessage.ServerResponse(id, Right(ServerResponseDetails.Data(token)))
      goto(State.Authenticated) using Data.UserConnection(userId, terminalId, connection)

    case Event(AuthenticationHandlingActor.Response.Failure(tpe), Data.Authentication(id, connection)) ⇒
      connection ! ServerMessage.ServerResponse(id, Left(tpe))
      goto(State.Unauthorized) using Data.Connection(connection)

    case Event(StateTimeout, Data.Authentication(id, connection)) ⇒
      connection ! ServerResponse(id, Left(FailureType.Timeout))
      goto(State.Unauthorized) using Data.Connection(connection)
  }

  onTransition {
    case State.Unauthorized -> State.Progress ⇒
      logger.info("State.Unauthorized -> State.Progress")
    case State.Progress -> State.Unauthorized ⇒
      logger.info("State.Progress -> State.Unauthorized")
    case State.Progress -> State.Authenticated ⇒
      logger.info("State.Progress -> State.Authenticated")
  }

  initialize()
}
