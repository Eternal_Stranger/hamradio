package org.stranger.hamradio.backend.domain

/**
  * Created by stranger on 12/3/2015.
  */

import akka.actor.{Actor, Props}
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.stranger.hamradio.backend.config.TestData
import org.stranger.hamradio.backend.protocol.FailureType

import scala.concurrent.Promise

object UsersActor {

  def props(): Props = {
    Props(classOf[UsersActor])
  }

  sealed trait Request
  object Request {
    case class Create(user: User) extends Request
    case class Retrieve(id: Id[User]) extends Request
    case class ResolveByName(name: String) extends Request
    case class ResolveByCredentials(name: String, password: String) extends Request
    case object List extends Request
  }

  sealed trait Response
  object Response {
    case class Failure(exception: FailureType) extends Response
    case class Resolved(result: Either[FailureType, User]) extends Response
    case class Created(result: Either[FailureType, User]) extends Response
  }

  private[UsersActor] sealed trait Event
  private[UsersActor] object Event {
    case class UserCreatedEvent(user: User) extends Event
  }

  private[UsersActor] case class State(users: Seq[User] = Seq()) {
    def updated(u: User): State = {
      this.copy(users = users :+ u)
    }
  }
}

class UsersActor extends Actor with LazyLogging {
  import UsersActor._

  private[UsersActor] var state = State()

  val initPromise = Promise[Boolean]()

  def persistenceId: String = "users-actor"

  override def preStart(): Unit = {
    super.preStart()
    loadInitialData()
  }

  override def receive: Receive = {
    case _ ⇒ logger.error(s"Unknown message incoming to user actor ")
  }

  private[UsersActor] def loadInitialData() = {
    TestData.users.foreach { user ⇒
      self ! Request.Create(user)
    }

    if (!initPromise.isCompleted) {
      initPromise.success(true)
    }
  }

}