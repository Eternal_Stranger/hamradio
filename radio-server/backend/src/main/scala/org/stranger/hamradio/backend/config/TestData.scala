package org.stranger.hamradio.backend.config

import org.stranger.hamradio.backend.domain.{Id, User}

object TestData {

  lazy val users = Seq(
    createUser("reg", "reg"),
    createUser("admin", "admin")
  )

  private[TestData] def createUser(login: String, password: String): User = {
    User(Some(Id[User]()), login, password)
  }

}
