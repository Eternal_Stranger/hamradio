package org.stranger.hamradio.backend.protocol

sealed trait FailureType extends Throwable
object FailureType {
  case object AccessDenied extends FailureType
  case object Unknown extends FailureType
  case object Duplicate extends FailureType
  case object RecordNotFound extends FailureType
  case object InvalidInput extends FailureType
  case object InvalidOutput extends FailureType
  case object UnknownMethod extends FailureType
  case object Timeout extends FailureType
  case object AlreadyAuthenticated extends FailureType
}
