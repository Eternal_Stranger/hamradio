package org.stranger.hamradio.backend

import akka.actor._
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.stranger.hamradio.backend.config.Settings
import org.stranger.hamradio.backend.network.TcpServer

object AppServer {


}

class AppServer(val settings: Settings)(implicit val system: ActorSystem) extends RootActors with LazyLogging {

  def start(): Unit = {
    
    logger.info("Starting TCP server..")
    val server = new TcpServer(system, settings.frontend.host, settings.frontend.port, this)
    server.start()

  }

}

