package org.stranger.hamradio.backend.config

import com.typesafe.config._

class Settings(config: Config = ConfigFactory.load()) {

  // Tcp-socket config
  lazy val frontend = new {
    val port = config.getInt("server.port")
    val host = config.getString("server.hostname")
  }

}
