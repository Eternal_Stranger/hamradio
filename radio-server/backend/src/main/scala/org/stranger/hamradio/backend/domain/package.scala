package org.stranger.hamradio.backend

import java.util.UUID

/**
  * Created by stranger on 12/3/2015.
  */
package object domain {


  case class Id[T]( id: String = UUID.randomUUID().toString )

  sealed trait DomainEntity

  case class User(id: Option[Id[User]], login: String, password: String) extends DomainEntity

}
