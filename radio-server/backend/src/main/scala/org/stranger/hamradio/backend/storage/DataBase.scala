package org.stranger.hamradio.backend.storage

import akka.actor.{Props, FSM}
import akka.util.Timeout
import scala.concurrent.duration._
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.stranger.hamradio.backend.storage.DataBase.{Data, State}

/**
  * @author A.Morozov
  */

object DataBase {

  def props(): Props = Props(classOf[DataBase])

  private[DataBase] sealed trait State

  private[DataBase] object State {
    case object Init extends State
    case object Connected extends State
  }

  private[DataBase] sealed trait Data

  private[DataBase] object Data {
    case object Init extends Data
    case class  Queue(seq: Seq[Int]) extends Data
   }
}

class DataBase extends FSM[State, Data] with LazyLogging {

  implicit val timeout = Timeout(5.seconds)

  override def postStop() = logger.info("Database closed")

  startWith(State.Init, Data.Init)

  whenUnhandled {
    case _ ⇒ stay()
  }

  when (State.Init) {
    case _ ⇒ stay()
  }

  when (State.Connected) {
    case _ ⇒ stay()
  }

  initialize()
}
