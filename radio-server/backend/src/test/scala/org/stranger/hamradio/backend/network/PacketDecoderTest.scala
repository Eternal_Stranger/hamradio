package org.stranger.hamradio.backend.network

import java.nio.ByteOrder

import akka.actor.{ActorRef, Props, Actor, ActorSystem}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.TestKit
import akka.util.{ByteString, Timeout}
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.stranger.hamradio.backend.RootActors
import org.stranger.hamradio.backend.config.Settings
import org.stranger.hamradio.backend.network.TcpServer.ConnectionOpen
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
  * @author A.Morozov
  */
class PacketDecoderTest(_system: ActorSystem) extends TestKit(_system)
                                              with WordSpecLike
                                              with Matchers
                                              with BeforeAndAfterAll
                                              with ScalaFutures
                                              with LazyLogging {

  implicit val timeout = Timeout(5 seconds)

  implicit val byteOrder = ByteOrder.BIG_ENDIAN

  def this() = this(ActorSystem("MySpec"))

  override def afterAll = {
    TestKit.shutdownActorSystem(system)
    system.terminate()
  }


  "Flow" must {
    "Encode and decode tcp packet" in {
      implicit val mat = ActorMaterializer()
      val server = new TcpServer(_system, "localhost", 13, new MockActors(new Settings))
      val sessionActor = system.actorOf(Props(classOf[ReflectionActor]))
      val source: Source[ByteString, Unit] = Source.single(inputPacket)
      val resultFuture: Future[ByteString] = source.via(server.socketFlow(sessionActor)).runWith(Sink.head)
      val result = Await.result(resultFuture, 100000.seconds)
      logger.info(result.toString)
      result shouldBe inputPacket.take(7)
    }
  }

  val inputPacket: ByteString = {
    val builder = ByteString.newBuilder
    builder.append(ByteString("HM"))
    builder.putInt(1)
    builder.putByte(4)
    builder.append(ByteString("HM"))
    builder.putInt(2)
    builder.putByte(8)
    builder.putByte(16)
    builder.result()
  }

  def process(bs: ByteString) = {
    logger.info(bs.toString())
  }
}

case class MockActors(val settings: Settings)(implicit val system:ActorSystem) extends RootActors

class ReflectionActor extends Actor with LazyLogging {
  var conn: ActorRef = null
  def receive = {
    case ConnectionOpen(_, connection) =>
      conn = connection
      logger.info("ConnectionOpen")
    case msg =>
      conn ! msg
  }
}

