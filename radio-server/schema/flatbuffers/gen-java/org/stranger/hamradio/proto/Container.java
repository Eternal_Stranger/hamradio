// automatically generated, do not modify

package org.stranger.hamradio.proto;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class Container extends Table {
  public static Container getRootAsContainer(ByteBuffer _bb) { return getRootAsContainer(_bb, new Container()); }
  public static Container getRootAsContainer(ByteBuffer _bb, Container obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public Container __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long id() { int o = __offset(4); return o != 0 ? (long)bb.getInt(o + bb_pos) & 0xFFFFFFFFL : 0; }
  public int timestamp() { int o = __offset(6); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public byte type() { int o = __offset(8); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public byte bodyType() { int o = __offset(10); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public Table body(Table obj) { int o = __offset(12); return o != 0 ? __union(obj, o) : null; }

  public static int createContainer(FlatBufferBuilder builder,
      long id,
      int timestamp,
      byte type,
      byte body_type,
      int body) {
    builder.startObject(5);
    Container.addBody(builder, body);
    Container.addTimestamp(builder, timestamp);
    Container.addId(builder, id);
    Container.addBodyType(builder, body_type);
    Container.addType(builder, type);
    return Container.endContainer(builder);
  }

  public static void startContainer(FlatBufferBuilder builder) { builder.startObject(5); }
  public static void addId(FlatBufferBuilder builder, long id) { builder.addInt(0, (int)(id & 0xFFFFFFFFL), 0); }
  public static void addTimestamp(FlatBufferBuilder builder, int timestamp) { builder.addInt(1, timestamp, 0); }
  public static void addType(FlatBufferBuilder builder, byte type) { builder.addByte(2, type, 0); }
  public static void addBodyType(FlatBufferBuilder builder, byte bodyType) { builder.addByte(3, bodyType, 0); }
  public static void addBody(FlatBufferBuilder builder, int bodyOffset) { builder.addOffset(4, bodyOffset, 0); }
  public static int endContainer(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
  public static void finishContainerBuffer(FlatBufferBuilder builder, int offset) { builder.finish(offset); }
};

