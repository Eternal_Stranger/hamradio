// automatically generated, do not modify

package org.stranger.hamradio.proto;

public final class PacketType {
  private PacketType() { }
  public static final byte Command = 0;
  public static final byte Event = 1;
  public static final byte SoundFrame = 2;

  private static final String[] names = { "Command", "Event", "SoundFrame", };

  public static String name(int e) { return names[e]; }
};

