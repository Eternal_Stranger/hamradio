// automatically generated, do not modify

package org.stranger.hamradio.proto;

public final class Any {
  private Any() { }
  public static final byte NONE = 0;
  public static final byte Command = 1;
  public static final byte Event = 2;
  public static final byte SoundFrame = 3;

  private static final String[] names = { "NONE", "Command", "Event", "SoundFrame", };

  public static String name(int e) { return names[e]; }
};

