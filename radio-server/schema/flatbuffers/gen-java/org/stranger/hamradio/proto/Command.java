// automatically generated, do not modify

package org.stranger.hamradio.proto;

import java.nio.*;
import java.lang.*;
import java.util.*;
import com.google.flatbuffers.*;

@SuppressWarnings("unused")
public final class Command extends Table {
  public static Command getRootAsCommand(ByteBuffer _bb) { return getRootAsCommand(_bb, new Command()); }
  public static Command getRootAsCommand(ByteBuffer _bb, Command obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public Command __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public int data(int j) { int o = __offset(4); return o != 0 ? bb.get(__vector(o) + j * 1) & 0xFF : 0; }
  public int dataLength() { int o = __offset(4); return o != 0 ? __vector_len(o) : 0; }
  public ByteBuffer dataAsByteBuffer() { return __vector_as_bytebuffer(4, 1); }

  public static int createCommand(FlatBufferBuilder builder,
      int data) {
    builder.startObject(1);
    Command.addData(builder, data);
    return Command.endCommand(builder);
  }

  public static void startCommand(FlatBufferBuilder builder) { builder.startObject(1); }
  public static void addData(FlatBufferBuilder builder, int dataOffset) { builder.addOffset(0, dataOffset, 0); }
  public static int createDataVector(FlatBufferBuilder builder, byte[] data) { builder.startVector(1, data.length, 1); for (int i = data.length - 1; i >= 0; i--) builder.addByte(data[i]); return builder.endVector(); }
  public static void startDataVector(FlatBufferBuilder builder, int numElems) { builder.startVector(1, numElems, 1); }
  public static int endCommand(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

