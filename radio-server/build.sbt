import sbtassembly.Assembly
import sbtassembly.AssemblyPlugin.autoImport._

name := "hamradio"

scalaVersion := Dependencies.Versions.scala

resolvers := Dependencies.resolvers

val doNotAssembly = Seq(assembly := file(""))

val commonSettings = Seq(
  version := "0.1.0-SNAPSHOT",
  organization := "org.stranger.hamradio",
  homepage := Some(url("https://bitbucket.org/Eternal_Stranger/hamradio")),
  scalaVersion := Dependencies.Versions.scala,
  scalacOptions ++= Seq(
    "-deprecation",
    "-feature",
    "-Xfatal-warnings",
    "-language:postfixOps",
    "-language:implicitConversions",
    "-language:reflectiveCalls"
  ),
  addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0-M5" cross CrossVersion.full),
  assembly := file(""),
  scalastyleFailOnError := true,
  compileOrder := CompileOrder.JavaThenScala
)

lazy val schema = (project in file("schema")).dependsOn(flatbuffers)
  .settings(commonSettings:_*)
  .settings(compileJavaSchemaTask)
  .settings(compile <<= compile in Compile dependsOn compileJavaSchema)
  .settings(javaSource in Compile := baseDirectory.value / "flatbuffers" / "gen-java")

lazy val flatbuffers = (project in file("flatbuffers"))
  .settings(commonSettings:_*)
  .settings(version := "0.2.0")
  .settings(publish := {})

lazy val compileJavaSchema = taskKey[Unit]("Run flatc compiler to generate Java classes for schema")
lazy val compileJavaSchemaTask = compileJavaSchema := {
  val schemaFiles = "ls -1 ../protocol/".!!.split("\n")
  schemaFiles.filter(_.endsWith(".fbs")).foreach { schema =>
    val result = s"flatc -j -o schema/flatbuffers/gen-java ../protocol/$schema".!!
    println(s"*** Generated Java classes from FlatBuffer schema $schema.  Results: $result")
  }
}


lazy val backend = project.
  settings(name := "hamradio-backend").
  settings(doNotAssembly:_*).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Dependencies.backend.value
  ).
  settings(
    assembly := Assembly.assemblyTask(assembly).value,
    assemblyJarName in assembly := "hamradio-server.jar",
    test in assembly := {},
    mainClass in assembly := Some("org.stranger.hamradio.Main"),
    assemblyMergeStrategy in assembly := {
      case PathList("org", "slf4j", _*) |
           PathList("ch.qos", "logback", _*) |
           PathList("org", "apache", _*) |
           PathList("org", "iq80", _*) |
           PathList("org", "xmlpull", _*) => MergeStrategy.first
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    }
  ).
  dependsOn(schema, flatbuffers, Dependencies.Projects.orientPersistence)

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  aggregate(schema, flatbuffers, backend)

// IntelliJ IDEA settings
ideExcludedDirectories := Seq(
  ".idea",
  ".idea_modules",
  "project/project/target",
  "project/target",
  "backend/target",
  "target"
) map { d ⇒ file(baseDirectory.value.getAbsolutePath + "/" + d) }


