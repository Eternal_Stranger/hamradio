#ifndef PACKPROTO_H
#define PACKPROTO_H

#include <QObject>
#include <queue>
#include "protoframe.h"

class ProtoPacket: public QObject
{
    Q_OBJECT
public:
    ProtoPacket(QObject *parent=0);
private:


signals:
    void protoPacketReceived(QByteArray packet);
    void networkEror(QString msg);
public slots:
    void sendPacket(QByteArray packet);
    void reconnect();
};

#endif // PACKPROTO_H
