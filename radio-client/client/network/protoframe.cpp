#include "protoframe.h"
#include <QDataStream>
#include <QIODevice>
#include <QStringList>

ProtoFrame::ProtoFrame(QObject *parent) : QObject(parent)
{
    socket = new QTcpSocket(this);

    connect(socket, SIGNAL(connected()),this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()),this, SLOT(disconnected()));
    connect(socket, SIGNAL(bytesWritten(qint64)),this, SLOT(bytesWritten(qint64)));
    connect(socket, SIGNAL(readyRead()),this, SLOT(readyRead()));
    connect(this, SIGNAL(test(QString)), this, SLOT(onEvent(QString)));

//    connect(this, SIGNAL(eventPacket(QByteArray)),this, SLOT(sendPacket(QByteArray)));
}


void ProtoFrame::doConnect()
{
    if(attempts > 3) {
        return;
    }
    emit eventMsg("Crete socket...");


    qDebug() << "connecting...";
    emit test("Win test");

    // this is not blocking call
    socket->connectToHost("127.0.0.1", 8815);

    // we need to wait...
    if(!socket->waitForConnected(30000))
    {
        qDebug() << "Error: " << socket->errorString();
        emit eventMsg("Network error:");
        emit eventMsg(socket->errorString());
    }
}

void ProtoFrame::doDisconnect()
{
    socket->close();
//    socket->disconnect();
}

void ProtoFrame::onEvent(QString msg) {
    emit eventMsg(msg);
}

void ProtoFrame::connected()
{
    qDebug() << "connected...";
    emit eventMsg("socket connected...");
//    // Hey server, tell me about you.
//    socket->write("HEAD / HTTP/1.0\r\n\r\n\r\n\r\n");
    sendPacket(QByteArray("Hello frame!"));
}

void ProtoFrame::disconnected()
{
     emit eventMsg("socket disconnected...");
    qDebug() << "disconnected...";
}

void ProtoFrame::bytesWritten(qint64 bytes)
{
     emit eventMsg("bytes written...");
    qDebug() << bytes << " bytes written...";
}

void ProtoFrame::sendPacket(const QByteArray &packet)
{
    QByteArray outBuffer;
    QByteArray header;
    header.append('H');
    header.append('M');
    QDataStream out(&outBuffer, QIODevice::WriteOnly);
    out << packet;
    socket->write(header);
    socket->write(outBuffer);
}

void ProtoFrame::readyRead()
{
    QDataStream in(socket);
    qDebug() << "reading...";
    emit eventMsg("reading...");    
    if(packet) {
        if(socket->bytesAvailable() < 6) {
            return;
        } else {
            QByteArray header;
            header = socket->read(2);
            if(header.at(0)=='H'&&header.at(1)=='M') {
                packet = false;
                in >> size;
            } else {
                attempts ++;
                doDisconnect();
                doConnect();
            }
        }
    }
    qDebug() << socket->bytesAvailable();

    if(socket->bytesAvailable() >= size) {
        QByteArray packetBuffer;
        packetBuffer = socket->read(size);
        emit eventPacket(packetBuffer);
        size = 0;
        packet = true;
        emit eventMsg("Packet:");
        emit eventMsg(packetBuffer);
    }
}
