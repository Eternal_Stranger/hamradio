#ifndef NETWORKCLIENT_H
#define NETWORKCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QDebug>

class ProtoFrame: public QObject
{
    Q_OBJECT
public:
    explicit ProtoFrame(QObject *parent = 0);
//    ~NetworkClient();
    void doConnect();
    void doDisconnect();

signals:

public slots:
    void connected();
    void disconnected();
    void bytesWritten(qint64 bytes);
    void sendPacket(const QByteArray &packet);
    void readyRead();

signals:
   void eventMsg(QString msg);
   void eventPacket(const QByteArray &packet);
   void test(QString msg);

private slots:
   void onEvent(QString msg);

private:
    QTcpSocket *socket;
    quint32 attempts = 0;
    bool packet = true;
    qint32 size = 0;
};

#endif // NETWORKCLIENT_H
