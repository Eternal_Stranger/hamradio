#include <QGuiApplication>
#include <QDebug>
#include <QtNetwork>

#include <qqmlengine.h>
#include <qqmlcontext.h>
#include <qqml.h>
#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>

#include "network/protoframe.h"
#include "location/gpslocator.h"
#include "signals/message.h"
#include "audio/audioprocessor.h"
#include "workers/mainworker.h"

#include "hamradio_generated.h"

using namespace org::stranger::hamradio::proto;

int main(int argc, char *argv[])
{
    qDebug() << "Mobile application starting...";
    //Q_DECLARE_METATYPE(Message)

    QGuiApplication app(argc, argv);

    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    QQmlContext *ctxt = view.rootContext();

    Message msg;
    ctxt->setContextProperty("Receiver", &msg);

    view.setSource(QUrl("qrc:view.qml"));
    //view.setGeometry(QRect(100, 100, 360, 480));
    view.show();


    msg.send("test message received");

   // QQmlApplicationEngine engine;
    //engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    qDebug() << "Network requset sending..";



    QThread* thread = new QThread();
    MainWorker *worker = new MainWorker(); 
    QObject::connect(thread, SIGNAL(started()), worker, SLOT(process()));
    QObject::connect((QObject*)view.engine(),SIGNAL(quit()),thread,SLOT(quit()));//stop thread on close
    QObject::connect(worker, SIGNAL(eventMsg(QString)), &msg, SLOT(rcv(QString)), Qt::QueuedConnection);
    worker->moveToThread(thread);
    thread->start();

    flatbuffers::FlatBufferBuilder fbb;
    auto name = fbb.CreateString("Container");
    unsigned char inv[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    auto inventory = fbb.CreateVector(inv, 10);
//    Vec3 vec(1, 2, 3);
    auto mloc = CreateContainer(fbb, 1, 1);
    FinishContainerBuffer(fbb, mloc);

    auto ptr = fbb.GetBufferPointer();
    auto monster = GetContainer(ptr);
    qDebug() << "serializzed monster: " << QString() << monster->id();
//    auto pos = monster->pos();
//    assert(pos);

    return app.exec();
}


