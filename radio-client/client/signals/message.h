#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>

class Message : public QObject
{
    Q_OBJECT
public:
    explicit Message(QObject *parent = 0);
    void send(QString body);

signals:
    void messageReceived(QString body);

public slots:
    void rcv(QString body);
};

#endif // MESSAGE_H
