#include "message.h"
#include <QDebug>

Message::Message(QObject *parent) : QObject(parent)
{

}

void Message::send(QString body) {\
    qDebug() << "Send message: " << body;
    emit messageReceived(body);
}

void Message::rcv(QString body)
{
    emit messageReceived(body);
}
