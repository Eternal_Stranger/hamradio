#include "mainworker.h"
#include <QCoreApplication>
#include <QEventLoop>


MainWorker::MainWorker( QObject *parent) : QObject(parent)
{
}

void MainWorker::process()
{

//    gps = new GPSLocator(this);
//    QObject::connect(gps, SIGNAL(eventMsg(QString)), this, SLOT(rcv(QString)));
//    gps->wait();


    frame = new ProtoFrame(this);
    QObject::connect(frame, SIGNAL(eventMsg(QString)),  this, SLOT(rcv(QString)));
    frame->doConnect();
    emit eventMsg("test message from THREAD received");

//    audio = new AudioProcessor(this);
//    QObject::connect(audio, SIGNAL(eventMsg(QString)), this, SLOT(rcv(QString)));
//    audio->startProcessing();

    //run event message loop
   /* forever {
      QCoreApplication::processEvents( QEventLoop::AllEvents, 1);
    }*/
}

void MainWorker::rcv(QString msg) {
    qDebug() << "MainThread rcv " << msg;
    emit eventMsg(msg);
}

