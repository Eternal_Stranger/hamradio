#ifndef MAINTHREAD_H
#define MAINTHREAD_H

#include <QThread>
#include "signals/message.h"
#include "location/gpslocator.h"
#include "network/protoframe.h"
#include "audio/audioprocessor.h"

class MainWorker : public QObject
{
 Q_OBJECT
public:
    explicit MainWorker(QObject *parent = 0);
signals:
    void eventMsg(QString msg);

private:
    ProtoFrame* frame;
    GPSLocator* gps;
    AudioProcessor* audio;

public slots:
    void process();
    void rcv(QString msg);
};

#endif // MAINTHREAD_H
