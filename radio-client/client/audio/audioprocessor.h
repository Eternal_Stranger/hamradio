#ifndef AUDIOPROCESSOR_H
#define AUDIOPROCESSOR_H

#include <QObject>
#include <QByteArray>
#include <QBuffer>
#include <QAudioDeviceInfo>
#include <QAudioFormat>
#include <QAudioInput>
#include <QAudioOutput>

class AudioProcessor : public QObject
{
    Q_OBJECT
public:
    explicit AudioProcessor(QObject *parent = 0);
    ~AudioProcessor();

signals:
    void eventMsg(QString msg);

public slots:
    void startProcessing();

private:
    void initializeAudio();
    void createAudioInput();
    void createAudioOutput();

    int ApplyVolumeToSample(short iSample);

private slots:
    void readMore();
    void volumeChange(int value);

private:
    QAudioDeviceInfo m_Inputdevice;
    QAudioDeviceInfo m_Outputdevice;
    QAudioFormat m_format;
    QAudioInput *m_audioInput;
    QAudioOutput *m_audioOutput;
    QIODevice *m_input;
    QIODevice *m_output;
    QByteArray m_buffer;
    int m_iVolume;
};

#endif // AUDIOPROCESSOR_H
