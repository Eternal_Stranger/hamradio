import QtQuick 2.0

Item {
    id: root
    signal trigger
//![0]
    ListView {
        width: 100; height: 1100

        model: chatContent
        delegate: Rectangle {
            height: 35
            width: 200
            Text { text: modelData }
        }
    }
//![0]

    onTrigger: console.log("trigger signal emitted")

    function appendText(newText) {
        chatContent.append({content: newText})
    }

    ListModel {
        id: chatContent
        ListElement {
            content: "Ebanij client"
        }
    }

    Connections {
        target: Receiver
        onMessageReceived: {
            console.log("onMessageReceived: " + body)
            appendText(body)
        }
    }
}
