#ifndef GPSLOCATOR_H
#define GPSLOCATOR_H

#include <QObject>
#include <QGeoPositionInfo>
#include <QGeoPositionInfoSource>

class GPSLocator: public QObject
{
    Q_OBJECT
public:
    explicit GPSLocator(QObject *parent = 0);
    void wait();

signals:
    void eventMsg(QString msg);

private slots:
    void positionUpdated(const QGeoPositionInfo &update);
    void error(QGeoPositionInfoSource::Error &error);
    void updateTimeout();
};

#endif // GPSLOCATOR_H
