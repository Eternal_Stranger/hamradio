#include "gpslocator.h"
#include <QDebug>

GPSLocator::GPSLocator(QObject *parent) : QObject(parent)
{
    qDebug() << "GPS starting..";
    emit eventMsg("GPS starting...");
    QGeoPositionInfoSource *gps = QGeoPositionInfoSource::createDefaultSource(this);
    if (!gps)
        return;
    gps->setPreferredPositioningMethods(QGeoPositionInfoSource::AllPositioningMethods);
    connect(gps, SIGNAL(positionUpdated(QGeoPositionInfo)), this, SLOT(positionUpdated(QGeoPositionInfo)));
    connect(gps, SIGNAL(updateTimeout()), this, SLOT(updateTimeout()));
    connect(gps, SIGNAL(error(QGeoPositionInfoSource::Error)), this, SLOT(error(QGeoPositionInfoSource::Error)));
    gps->setUpdateInterval(1000 * 60 * 15);
    gps->startUpdates();
}

void GPSLocator::wait()
{
    qDebug() << "await";
    emit eventMsg("await");
}

void GPSLocator::positionUpdated(const QGeoPositionInfo &update)
{
    if (!update.isValid() || !update.coordinate().isValid()) {
        qDebug() << "invalid GPS location";
        return;
    }
    QString str;
    emit eventMsg("lat: " + QString::number(update.coordinate().latitude()) + " lon: " + QString::number(update.coordinate().longitude()));
    qDebug() << "timestamp: " << update.timestamp().toMSecsSinceEpoch();
    qDebug() << "altitude: " << update.coordinate().altitude();
    qDebug() << "lat: " << update.coordinate().latitude();
    qDebug() << "lon: " << update.coordinate().longitude();
}

void GPSLocator::error(QGeoPositionInfoSource::Error &error)
{
    emit eventMsg("GPS error: ");
    qDebug() << "GPS error: " << error;
}

void GPSLocator::updateTimeout()
{
     emit eventMsg("GPS updateTimeout");
     qDebug() << "GPS updateTimeout";
}
