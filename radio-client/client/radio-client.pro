TEMPLATE = app

QT +=   core \
        qml \
        quick \
        network \
        positioning \
        multimedia


CONFIG += c++11 orders
CONFIG += windeployqt

SOURCES += \
    location/gpslocator.cpp \
    main.cpp \
    signals/message.cpp \
    audio/audioprocessor.cpp \
    workers/mainworker.cpp \
    network/protoframe.cpp \
    network/protopacket.cpp

HEADERS += \
    location/gpslocator.h \
    signals/message.h \
    audio/audioprocessor.h \
    workers/mainworker.h \
    network/protoframe.h \
    network/protopacket.h

    network/packproto.h

INCLUDEPATH += /

#add a generated files dir in source so that we can link the symbol browser to it more easily.
GENERATED_FILES_DIR = $${_PRO_FILE_PWD_}/generated

FLATBUFFER_DEFINITIONS = \
    ../../protocol/hamradio.fbs

#configure the flatbuffer compiler
flatbuffers.input = FLATBUFFER_DEFINITIONS
flatbuffers.output = $${GENERATED_FILES_DIR}/flatbuffers/${QMAKE_FILE_BASE}_generated.h
flatbuffers.variable_out = HEADERS
flatbuffers.CONFIG += target_predeps no_link

unix {
    INCLUDEPATH +=  /usr/local/include/flatbuffers \
                $${GENERATED_FILES_DIR}/flatbuffers
    flatbuffers.commands = @echo "Compile flatbuffers.." && flatc -c -o $${GENERATED_FILES_DIR}/flatbuffers ${QMAKE_FILE_IN}
}
unix:mac {
    INCLUDEPATH +=  /usr/local/include/flatbuffers \
                $${GENERATED_FILES_DIR}/flatbuffers
    flatbuffers.commands = @echo "Compile flatbuffers.." && /usr/local/bin/flatc -c -o $${GENERATED_FILES_DIR}/flatbuffers ${QMAKE_FILE_IN}
}
win32 {
    INCLUDEPATH +=  \"../../protocol/include/flatbuffers\" \
                \"$${GENERATED_FILES_DIR}/flatbuffers\"
    flatbuffers.commands = @echo "Compile flatbuffers.." && \"$${_PRO_FILE_PWD_}/../../protocol/flatc.exe\" -c -o $${GENERATED_FILES_DIR}/flatbuffers ${QMAKE_FILE_IN}
}
QMAKE_EXTRA_COMPILERS += flatbuffers


RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

WINRT_MANIFEST.capabilities_device = location

DISTFILES += \
    AppxManifest.xml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

