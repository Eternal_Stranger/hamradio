# hamradio
Radio set over gprs

## Server 

```bash
sbt server/run
```

## Client

### Development

```bash
cp client/src/main/resources/js/configuration.js.ex client/src/main/resources/js/configuration.js
sbt fastOptJS && http-server ./client/target
```
Open in browser `http://localhost:8080/scala-2.11/classes/index-dev.html`

**Before creating pull-request**

1. Check style, run unit-tests:

    ```sbt scalastyle test:scalastyle clean client/test server/test server/run```


### Production

```bash
sbt fullOptJS && http-server ./client/target
```
Open in browser `http://localhost:8080/scala-2.11/classes/index.html`

## Assembly server

```bash
sbt assembly
```